# Bulkr
Bulkr is a tiny utility for running a command on multiple machines using passwordless SSH.

## Help
    -h/--help   Displays this help.
    -f          Specify a file with <user>@<hostname> lines for the target machines.
    <none>      Takes a <user>@<hostname> as the second argument and 'command' as the third for a one-off.

## Disclaimer
you need to enable passwordless SSH on the machines you intend to run commands on. You can use the `ssh-copy-id` command to share your key with the target machine.