use std::net::{TcpStream};
use ssh2::Session;
use std::fs::File;
use std::io::{prelude::*, BufRead, BufReader};
use std::env;

fn main() {
    // read input arguments
    let args: Vec<String> = env::args().collect();

    if args.len() <= 2 {
        print_help();
        return;
    }
    
    // change actions depending on arguments
    match args[1].as_str() {
        "-f" => {
            // read hosts from file and execute command recursively
            let file = File::open(args[2].as_str()).unwrap();
            let buffer = BufReader::new(file);

            for (_index, line) in buffer.lines().enumerate() {
                let line = line.unwrap();

                println!("connecting to: {}", line);
                let line: Vec<&str> = line.split("@").collect();
                
                let mut session = set_host( line[1], line[0]);

                send_command(&mut session, args[3].as_str());
            }
        },

        "-s" => {
            if args.len() >= 4 {
                // execute one-off command on remote host
                let info: Vec<&str> = args[2].split("@").collect();
                let mut session = set_host( info[1], info[0]);
    
                send_command(&mut session, args[3].as_str());
            } else {
                eprintln!("Err: not enough arguments specified");
            }
        },

        "-h"|"--help"|_ => {
            print_help();
        }

    }
}

fn set_host( hostname: &str, username: &str) -> Session{
    // Init the ssh session object
    let mut session = Session::new().unwrap();
    let tcp = TcpStream::connect(format!("{hostname}:22")).unwrap();
    session.set_tcp_stream(tcp);
    session.handshake().unwrap();
    session.userauth_agent(username).unwrap();

    return session;
}


fn send_command(session: &mut Session, command: &str) {
    let mut channel = session.channel_session().unwrap();
    channel.exec(command).unwrap();
    let mut s = String::new();

    let read_size = channel.read_to_string(&mut s).unwrap_or(0 as usize);

    if read_size == 0 {
        return;
    }
    println!("{}", &s);
    channel.close().unwrap();
}

fn print_help() {
    println!("

                == Welcome to Bulkr! A utility to run commands over SSH in bulk. ==\n
                ------------------------------------------------------------------\r
                This program only supports the use of passwordless ssh.\r
                You can use 'ssh-copy-id' to copy your key to the target machines.\r
                ------------------------------------------------------------------\n
                -h/--help   Displays this help.\r
                -f          Specify a file with <user>@<hostname> lines for the target machines.\r
                <none>      Takes a <user>@<hostname> as the second argument and 'command' as the third for a one-off.\n
            ");
}